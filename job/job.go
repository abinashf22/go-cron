package job

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"go.mongodb.org/mongo-driver/bson"

	"github.com/abinash393/go-cron/db"
)

// Exec func of cron job
func Exec() {
	log.Println("job is started")
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	curser, err := db.UserCollection.Find(ctx, bson.M{})
	defer curser.Close(ctx)
	if err != nil {
		panic(err.Error())
	}
	for curser.Next(ctx) {
		var user db.User
		if err := curser.Decode(&user); err != nil {
			panic(err)
		}

		id, err := primitive.ObjectIDFromHex(user.StatisticInfo)
		if err != nil {
			panic(err.Error())
		}

		db.StatCollection.FindOneAndUpdate(ctx, bson.M{"_id": id}, bson.M{
			"$set": bson.M{fmt.Sprintf(
				"followersStat." + time.Now().Local().Format("2006-01-02")): user.FollowersCount},
		})
	}

	log.Println("job is ended")
}
