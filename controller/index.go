package controller

import (
	"fmt"
	"net/http"

	"github.com/abinash393/go-cron/db"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// FollowerStat of the given user ID
func FollowerStat(c *gin.Context) {
	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		panic(err.Error())
	}

	var user db.User
	db.UserCollection.FindOne(c, bson.M{"_id": id}).Decode(&user)

	statID, err := primitive.ObjectIDFromHex(user.StatisticInfo)
	var stat db.Stat
	db.StatCollection.FindOne(c, bson.M{"_id": statID}).Decode(&stat)
	fmt.Println(stat)
	c.JSON(http.StatusOK, gin.H{"data": stat})
}
