package db

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// Client mongodb
var Client *mongo.Client

// User model
type User struct {
	ID             primitive.ObjectID `json:"id" bson:"_id"`
	FollowingCount int                `json:"followingCount" bson:"followingCount"`
	FollowersCount int                `json:"followersCount" bson:"followersCount"`
	StatisticInfo  string             `json:"statisticInfo" bson:"statisticInfo"`
	CreatedAt      string             `json:"createdAt" bson:"createdAt"`
}

// Stat model
type Stat struct {
	ID            primitive.ObjectID `json:"id,omitempty" bson:"_id"`
	FollowersStat map[string]int32   `json:"followersStat" bson:"followersStat"`
}

// UserCollection of cron db
var UserCollection *mongo.Collection

// StatCollection of cron db
var StatCollection *mongo.Collection

// TotalUser in UserCollections
var TotalUser int
