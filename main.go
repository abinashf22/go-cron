package main

import (
	"context"
	"log"
	"os"
	"sync"
	"time"

	"github.com/abinash393/go-cron/controller"
	"github.com/abinash393/go-cron/job"

	"github.com/gin-gonic/gin"

	"github.com/abinash393/go-cron/db"
	"github.com/joho/godotenv"
	"github.com/whiteshtef/clockwork"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const week = time.Hour * 24 * 7

func init() {
	godotenv.Load()

	var ctx, cancel = context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(
		os.Getenv("MONGO_URI"),
	))
	if err != nil {
		log.Fatal(err)
	} else {
		log.Println("MongoDB connected")
	}
	// echo.Start(os.Getenv("PORT"))
	database := client.Database("cron")
	db.UserCollection = database.Collection("users")
	db.StatCollection = database.Collection("stats")
	db.Client = client
}

var wg sync.WaitGroup

func main() {
	fakeUserGenarator(20)
	var ctx, cancel = context.WithCancel(context.Background())
	defer cancel()
	defer db.Client.Disconnect(ctx)

	sched := clockwork.NewScheduler()
	sched.Schedule().Every(1).Minutes().Do(job.Exec)
	// sched.Schedule().Every(1).Weeks().Do(job.Exec)
	go sched.Run()
	wg.Add(1)

	r := gin.New()
	r.Use(gin.Recovery())

	r.GET("/", func(c *gin.Context) {
		c.File("view/index.html")
	})
	r.Static("/static", "./static")
	api := r.Group("/api")
	{
		api.GET("/data/:id", controller.FollowerStat)
	}

	log.Fatal(r.Run(os.Getenv("PORT")))
	wg.Wait()
}

func fakeUserGenarator(n int) {
	log.Println("Generating fake data")
	for i := 1; i <= n; i++ {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		insertResult, err := db.StatCollection.InsertOne(ctx, bson.M{
			"followersStat": bson.D{},
		})

		if err != nil {
			log.Println(err.Error())
		}

		db.UserCollection.InsertOne(ctx, bson.M{
			"followingCount": 0,
			"followersCount": 0,
			"createdAt":      time.Now().Local().Format(time.RFC3339),
			"statisticInfo":  insertResult.InsertedID,
		})
	}
	log.Println("Data has been generated")
}
